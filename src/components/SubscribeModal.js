import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const SubScribeModal = props => {
    const [success, setSuccess] = useState(false)
    const toggleSuccess = state => {
        setSuccess(true)
        setTimeout(() => props.toggle(), 1000)
    }

    return (
        <div>
            <Modal isOpen={props.isOpen} toggle={props.toggle}>
                <ModalHeader toggle={props.toggle}>Sign Up</ModalHeader>
                <ModalBody>
                    <form>
                        <div className="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                            <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                    </form>
                    { success ? <h2>Success!</h2> : '' }
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={toggleSuccess}>Subscribe</Button>{' '}
                    <Button color="secondary" onClick={props.toggle}>Cancel</Button>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default SubScribeModal