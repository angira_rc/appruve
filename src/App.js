import React, { useState } from 'react'
import { Container } from 'reactstrap'
import SubScribeModal from './components/SubscribeModal'
import './App.css'

const App = () => {
    const [isOpen, setIsOpen] = useState(false)

    return (
        <div className="App">
            <Container className="title">
                <h1>Pricing</h1>
                <p>Please see the coding challenge below. You have 2 hours to complete it. When you are done reply to this email with the github repository link.</p>
            </Container>
            <Container className="pricing">
                <div className="pricing-row">
                    <div className="plan-card">
                        <h2>Bronze</h2>
                        <h1>KES 5,000/mo</h1>
                        <ul>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                        </ul>
                        <p onClick={ () => setIsOpen(!isOpen) } className="button">Subscribe</p>
                    </div>
                    <div className="plan-card active-card">
                        <h2>Silver</h2>
                        <h1>KES 10,000/mo</h1>
                        <ul>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                        </ul>
                        <p onClick={ () => setIsOpen(!isOpen) } className="button">Subscribe</p>                                       
                    </div>
                    <div className="plan-card">                        
                        <h2>Gold</h2>
                        <h1>KES 15,000/mo</h1>
                        <ul>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                            <li>Please see the coding challenge below.</li>
                        </ul>
                        <p onClick={ () => setIsOpen(!isOpen) } className="button">Subscribe</p>           
                    </div>
                </div>
            </Container>
            <SubScribeModal toggle={ () => setIsOpen(!isOpen) } isOpen={ isOpen } />
        </div>
    )
}

export default App
